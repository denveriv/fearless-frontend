import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [location, setLocation] = useState([]);


    const [name, setName] = useState("");
    function handleNameChange(event) {
        const value = event.target.value;
        setName(value);
    }

    const [start, setStart] = useState("");
    function handleStartChange(event) {
        const value = event.target.value;
        setStart(value);
    }

    const [end, setEnd] = useState("");
    function handleEndChange(event) {
        const value = event.target.value;
        setEnd(value);
    }

    const [description, setDescription] = useState("");
    function handleDescriptionChange(event) {
        const value = event.target.value;
        setDescription(value);
    }
    const [max_presentations, setMaxPresent] = useState("");
    function handleMaxPresentChange(event) {
        const value = event.target.value;
        setMaxPresent(value);
    }

    const [maxAttendees, setMaxAttendee] = useState("");
    function handleMaxAttendeeChange(event) {
        const value = event.target.value;
        setMaxAttendee(value)
    }

    const [confLocation, setConfLocation] = useState("");
    function handleLocationChange(event) {
        const value = event.target.value;
        setConfLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = maxAttendees;
        data.location = confLocation;
        console.log(data);

        const conferenceUrl = "http://localhost:8000/api/conferences/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        }

        const response = await fetch(conferenceUrl, fetchConfig);
        if(response.ok){
            const newConference = await response.json();
            console.log(newConference);

            setName("");
            setStart("");
            setEnd("");
            setDescription("");
            setMaxPresent("");
            setMaxAttendee("");
            setConfLocation("");
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setLocation(data.locations)
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Conference</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" className="form-control" value={name} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartChange} placeholder="Starts" required type="date" id="starts" className="form-control" value={start} />
                            <label htmlFor="starts">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndChange} placeholder="Ends" required type="date" id="ends" className="form-control" value={end}/>
                            <label htmlFor="ends">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleDescriptionChange} className="form-control" placeholder="Description" id="description" style={{height: '7em' }} value={description} ></textarea>
                            <label htmlFor="description">Description</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresentChange}placeholder="Maximum Presentations" required type="number" id="max_presentations"
                                className="form-control" value={max_presentations}/>
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendeeChange}placeholder="Maximum Attendees" required type="number" id="max_attendee"
                                className="form-control" value={maxAttendees}/>
                            <label htmlFor="max_attendee">Max Attendees</label>
                        </div>
                        <p> Choose Location </p>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required id="location" className="form-select" value={confLocation}>
                                <option value="">Choose a Location</option>
                                {location.map(location => {
                                    return (
                                        <option key={location.name} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    )


}

export default ConferenceForm;
