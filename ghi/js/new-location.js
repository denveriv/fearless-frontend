console.log("Hello There");

function createAlert(error){
    return `<div class="alert alert-danger d-flex align-items-center" role="alert">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
      <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </svg>
    <div>
      <p>${error}</p>
    </div>
  </div>`
}




// We need to add an event listener for when the DOM loads."
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states';

    // "Let's declare a variable that will hold the URL for the API that we just created."
    try {
        const response = await fetch(url);

        if (!response.ok) {
            const htmlError = createAlert();
            const column = document.querySelector(".row");
            column.innerHTML += htmlError;
            // Figure out what to do when the response is bad
        } else {
            const states = await response.json();
            const selectTag = document.getElementById("state")
            for (const state of states.states){
                console.log(state);
                
            }

        }

    } catch (e) {
        console.log("e");
        const htmlError = createAlert(e);
        const column = document.querySelector(".row");
        column.innerHTML += htmlError;
    }

});


// Get the select tag element by its id 'state'

// For each state in the states property of the data

  // Create an 'option' element

  // Set the '.value' property of the option element to the
  // state's abbreviation

  // Set the '.innerHTML' property of the option element to
  // the state's name

  // Append the option element as a child of the select tag
