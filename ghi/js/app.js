function createCard(name, description, pictureUrl, startDate, endDate, locNam) {
    return `
    <div class="col mt-3">
    <div class="card shadow">
    <img src="${pictureUrl}" class="card-img-top">
    <div class="card-body">
        <h5 class ="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${locName}</h6>
        <p class="card-text">${description}</p></div>
        <div class="card-footer text-body-secondary">${startDate} - ${endDate}
    </div>
    </div>
    </div>`;
}

function createAlert(){
    return `<div class="alert alert-danger d-flex align-items-center" role="alert">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
      <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </svg>
    <div>
      <p>There was an Error loading Conferences</p>
    </div>
  </div>`
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const htmlError = createAlert();
            const column = document.querySelector(".row");
            column.innerHTML += htmlError;
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const startDate = new Date(details.conference.starts).toLocaleDateString('en');
                    const endDate = new Date(details.conference.ends).toLocaleDateString('en');
                    console.log(details);
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const locName = details.conference.location.name
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, locName);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }

        }
    } catch (e) {
        console.log("e");
        const htmlError = createAlert();
        const column = document.querySelector(".row");
        column.innerHTML += htmlError;
    }

});
